FROM alpine:3.13.0

WORKDIR /tmp/install-tl
RUN apk update && apk add --no-cache texmf-dist texlive-full \
    git && \
    cd && rm -rf /tmp/install-tl

